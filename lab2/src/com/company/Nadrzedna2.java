package com.company;

abstract class Nadrzedna2{
    abstract void print();

    Nadrzedna2(){
        print();
    }
}

class Podrzedna2 extends Nadrzedna2{
    int x = 3;
    @Override
    public void print(){
        System.out.println("x = " + x);
    };
}
