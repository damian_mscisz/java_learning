package com.company;

public class Nadrzedna{

    public void pierwszaMetoda(){
        System.out.println("Jestem metoda pierwsza klasa pierwsza");
        drugaMetoda();
    }

    public void drugaMetoda(){
        System.out.println("Jestem metoda druga klasa pierwsza");
    }
}

class Podrzedna extends Nadrzedna{
    @Override
    public void drugaMetoda(){
        System.out.println("Jestem metoda druga klasa druga");
    }
}

/*
* @Override daje sie gdy tworze w klasie pdorzednej metoda ktore przeslaniam. Pozwala tylko uniknac
* */

/*

* slowo kluczowe super.funkcja pozwala nam na wywolanie  metody najpierw z klasy nadrzednej a pozniej z automatu
* wywoluje sie metoda z klasy podrzednej
*
* */


