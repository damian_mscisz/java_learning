package com.company;

import java.awt.*;

public class SomethingIsWrong {

    public static void main(String[] args) {
        Rectangle myRect = new Rectangle();
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.getWidth()*myRect.getHeight());
    }
}

/*
* aby przyklad dzialal prawidlowo nalezalo utworzyc poprawnie obiekt typu Rectangle w linijce 8
* oraz poprawic obliczanie powierzchni prostokatu.
* zaimportowac rowniez import java.awt.*
* nie znalazlem takiej metody jak .area() w klasie Rectangle
* */
