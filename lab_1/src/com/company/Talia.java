package com.company;
import java.util.ArrayList;
import java.util.List;

public class Talia {
    List <Karta> taliaKart = new ArrayList<Karta>();
    String figury[] = {"2","3","4","5","6","7","8","9","10","As","Jupek","Krol","Dama"};
    String kolor[] = {"czerwo","krajc","dzwonek","wino"};

    int lengthKolorArray = kolor.length;
    int lengthFiguryArray = figury.length;
    int i = 0;
    int j = 0;

    public Talia(){
        for(int i=0;i<lengthKolorArray;i++){
            for(int j=0;j<lengthFiguryArray;j++){
                taliaKart.add(new Karta(kolor[i],figury[j]));
            }
        }
    }

    public void WypiszKarty(){
        for(int i=0; i<52;i++){
            System.out.println(taliaKart.get(i).kolor+"-"+taliaKart.get(i).figura);
        }
    }
}
