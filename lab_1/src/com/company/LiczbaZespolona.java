package com.company;

public class LiczbaZespolona {
    float a;
    float b;

    public void Wypisz(){
        System.out.println(a + "+" + b + "i");
    }

    public void UstawWartosc(float pA, float pB){
        this.a = pA;
        this.b = pB;
    }

    /*dlatego ta funkcja bo nie odwoluje sie do pol oraz metod niestatycnych klasy*/
    public static LiczbaZespolona Dodaj(LiczbaZespolona z1, LiczbaZespolona z2){
        LiczbaZespolona tmp = new LiczbaZespolona();
        tmp.a = z1.a + z2.a;
        tmp.b = z1.b + z2.b;
        return tmp;
    }

    /*
    *  Z wnętrza metody statycznej Nie możemy odwoływać się do pól i metod nie zadeklarowanych jako statyczne.
    *  Dodatkowo metody i zmienne zadeklarowane jako static związane są z konkretną klasą,
    *  a Metody statyczne podobnie jak statyczne pola danych są przypisane do klasy
    *  a nie konkretnego obiektu i służą do operacji tylko na polach statycznych.nie jej instancją - obiektem.
    * */
}
