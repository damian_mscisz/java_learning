###Lab 1

- (2 punkty) Zaimplementuj w języku Java klasę, której obiekty będą reprezentować pojedyncze karty z talii (nazwa klasy: Karta). Podstawowe własności każdej karty to: kolor i figura, niezmienne dla każdego obiektu. Następnie stwórz klasę, której obiekt będzie reprezentował całą talię (nazwa klasy: Talia). Obiekt ten ma zawierać 52 utworzone obiekty klasy Karta przechowywane w tablicy. Do obiektu Talia dodaj metodę wypisującą wszystkie karty (nazwa metody: WypiszKarty) w postaci „kolor - figura”. W metodzie main utwórz obiekt klasy Talia i wywołaj jego metodę WypiszKarty.



- (2 punkty) Stwórz klasę reprezentującą liczby zespolone (nazwa klasy: LiczbaZespolona). Każda liczba zespolona może być reprezentowana przez dwie liczby rzeczywiste a i b. Utwórz następujące metody:
	• Wypisz() – wypisuje liczbę zespoloną w postaci a+ib
	• UstawWartosci(float a, float b) – ustawia wartości a i b liczby zespolonej.
	• LiczbaZespolona Dodaj(LiczbaZespolona z1, LiczbaZespolona z2) – dodaje do siebie dwie liczby zespolone i zwraca wynik (nie wypisuje go na ekran). Tą metodę, która najlepiej się do tego nadaje ustaw jako statyczną. Dostęp do zmiennych a i b ma być możliwy jedynie z wewnątrz klasy LiczbaZespolona oraz z ewentualnych klas pochodnych. Utwórz dwa obiekty klasy LiczbaZespolona. Ustaw wartości pierwszej na a=1.0; b=1.0. A wartości drugiej na: a=2.0; b=2.0. Następnie wypisz obie liczby, dodaj je do siebie i wypisz liczbę wynikową.


- (1 punkt) Utwórz klasę Licznik. Zaimplementuj w niej mechanizm zliczania ilości utworzonych obiektów tej klasy. W metodzie main utwórz 5 obiektów klasy Licznik i zademonstruj, że mechanizm zliczania ilości obiektów działa poprawnie.